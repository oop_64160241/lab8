public class Map {
    private int width;
    private int height;

    public Map() {
        this.width = 5;
        this.height = 5;
    }
    public Map(int width, int height) {
        this.width = width;
        this.height = height;
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public void print() {
        for(int row=0; row<this.height; row++) {
            for(int col=0; col<this.height; col++) {
                System.out.print('-');
            }
            System.out.println();
        }
    }
}
