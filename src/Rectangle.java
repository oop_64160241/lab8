public class Rectangle {
    private double width;
    private double height;
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    public double getWidth() {
        return width;
    }
    public double getHeight() {
        return height;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public void seHeight(double height) {
        this.height = height;
    }
    public double calArea() {
        return width * height;
    }
    public double calPerimeter() {
        return (width+height) * 2;
    }
    public void printArea() {
        System.out.println("Perimeter: " + calPerimeter());
    }
    public void printPerimeter() {
    }
}
