public class TestTree {
    public static void main(String[] args) {
        Tree tree1 = new Tree(5, 10);
        Tree tree2 = new Tree(5, 11);
        tree1.print();
        tree2.print();
        for (int y = 0; y < 15; y++) {
            for (int x = 0; x < 15; x++) {
                if(tree1.getX()==x && tree1.getY()==y) {
                    System.out.print(tree1.getSymbol());
                    continue;
                }
                if(tree2.getX()==x && tree1.getY()==y) {
                    System.out.print(tree2.getSymbol());
                    continue;
                }
                System.out.print('-');
            }
            System.out.println();
        }
    }
}
