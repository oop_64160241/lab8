public class Circle {
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double getRadius() {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double calArea() {
        return Math.PI * radius *  radius;
    }
    public double calPerimeter() {
        return 2*Math.PI * radius;
    }

    public void printArea() {
        System.out.println("Area: " + calArea());
    }
    public void printPerimeter() {
        System.out.println("Perimeter: " + calPerimeter());
    }
}