public class Tree {
    private char symbol;
    private int x;
    private int y;
    public Tree(int x, int y) {
        this.symbol = 'T';
        this.x = x;
        this.y = y;
    }
    public int getX() { return x; }
    public int getY() { return y; }
    public char getSymbol() { return symbol; }
    public void print() {
        System.out.println("Tree " + symbol + " (" + x + "," + y + ")");
    }
}
