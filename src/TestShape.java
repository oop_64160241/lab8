public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        Rectangle rect2 = new Rectangle(5, 3);
        Circle circle1 = new Circle(1);
        Circle circle2 = new Circle(2);
        Triangle triangle1 = new Triangle(5, 5, 6);
        rect1.printArea();
        rect2.printArea();
        rect1.printPerimeter();
        rect2.printPerimeter();
        System.out.println("Rect1 Area: " + rect1.calArea());
        System.out.println("Rect2 Area: " + rect1.calArea());
        System.out.println("Circle1 Area: " + circle1.calArea());
        System.out.println("Circle2 Area: " + circle2.calArea());
        System.out.println("Triangle1 Area: " + Triangle1.calArea());

        System.out.println("Rect1 Perimeter: " + rect1.calPerimeter());
        System.out.println("Rect2 Perimeter: " + rect2.calPerimeter());
        System.out.println("Circle1 Perimeter: " + circle1.calPerimeter());
        System.out.println("Clrcle2 Perimeter: " + circle2.calPerimeter());
        System.out.println("Triangle1 Perimeter: " + circle2.calPerimeter());
    }  
}
